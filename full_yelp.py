# -*- coding: utf-8 -*-
"""Full_YELP.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1LuqZL6vsc-QdAjxUxwKvJbsKD4dFrpvS
"""

# Commented out IPython magic to ensure Python compatibility.
# %tensorflow_version 2.x
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Embedding, SimpleRNN, LSTM, GRU, SpatialDropout1D
from tensorflow.keras import utils
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.callbacks import ModelCheckpoint
import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import spacy
import re
from collections import Counter
# %matplotlib inline

#setting length of a review for learning and amount of words which we will tokenize
max_review_len = 150
dic_size = 13000

#downloading data
!wget https://s3.amazonaws.com/fast-ai-nlp/yelp_review_full_csv.tgz

#extracting the archive
!tar -xvf yelp_review_full_csv.tgz

#creating train and test data frames
train = pd.read_csv('yelp_review_full_csv/train.csv',
                    header=None,
                    names=['Class', 'Review'])

test = pd.read_csv('yelp_review_full_csv/test.csv',
                    header=None,
                    names=['Class', 'Review'])

#preprocessing reviews
reviews = train['Review'].apply(lambda review: re.sub(r"(\\n+)", " ", review))
reviews_test = test['Review'].apply(lambda review: re.sub(r"(\\n+)", " ", review))
#making answer classes to be 0 and 1 instead of 1 and 2
y_train = train['Class'] - 1
y_test = test['Class'] - 1

#transfering results to categorical form expected by neural network
y_train = utils.to_categorical(y_train, 5)
y_test = utils.to_categorical(y_test, 5)

#using spacy for tokenization of the reviews 
nlp = spacy.load('en_core_web_sm')
tokenizer = Tokenizer(nlp.vocab)
docs = nlp.pipe(reviews, batch_size=50, disable=["tagger", "parser", 'ner', 'textcat'])

#making a dictionary token:number of apperances in reviews
dic_counter = {}
token_reviews = []
for doc in docs:
    token_review = []
    for token in doc[0:max_review_len]:
        token_review.append(token.text)
        if token.text not in dic_counter.keys():
            dic_counter[token.text] = 1
        else:
            dic_counter[token.text] = dic_counter.get(token.text) + 1
    token_reviews.append(token_review)

#creating a dictionary with most popular tokens
word_bag_counter = Counter(dic_counter)
word_bag_counter = word_bag_counter.most_common(dic_size)
word_bag_seq = {word_bag_counter[i][0] : i+1 for i in range(dic_size)}

#The function turning review into a sequence of ints
def seq2int(str_sequence, mode="list"):
    assert mode in ["list", "token", "string"], "Please choose one of three modes list, token, string"
    if mode == "list":
        tokens = str_sequence
        int_sequence = []
        for token in tokens[0:max_review_len]:
            if token in word_bag_seq.keys():
                int_sequence.append(word_bag_seq.get(token))
            else:
                int_sequence.append(0)
        return int_sequence
    elif mode == "token":
        tokens = str_sequence
        int_sequence = []
        for token in tokens[0:max_review_len]:
            if token.text in word_bag_seq.keys():
                int_sequence.append(word_bag_seq.get(token.text))
            else:
                int_sequence.append(0)
        return int_sequence
    else:
        tokens = nlp(str_sequence)
        int_sequence = []
        for token in tokens[0:max_review_len]:
            if token.text in word_bag_seq.keys():
                int_sequence.append(word_bag_seq.get(token.text))
            else:
                int_sequence.append(0)
        return int_sequence

# encoding all reviews to integers
int_sequence = [seq2int(review) for review in token_reviews]

#trimming reviews' lengh and filling short reviews with zeros
x_train = pad_sequences(int_sequence, maxlen=max_review_len)
print(len(reviews), len(int_sequence), len(x_train))

#Initiating the model
model = Sequential()
model.add(Embedding(dic_size+1, 100, input_length=max_review_len))
model.add(SpatialDropout1D(0.5))
model.add(LSTM(40, return_sequences=True))
model.add(LSTM(40))
model.add(Dense(5, activation='softmax'))

model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

# due to relearning of the networks we will catch the best models during the learning process
model_save_path = 'best_model.h5'
checkpoint_callback = ModelCheckpoint(model_save_path,
                                      monitor='val_accuracy',
                                      save_best_only=True,
                                      verbose=1)

#training model
history = model.fit(x_train,
                    y_train,
                    epochs = 15,
                    batch_size=512,
                    validation_split=0.2,
                    callbacks=[checkpoint_callback])

#ploting learning progress
plt.plot(history.history['accuracy'], label='accuracy on traning data')
plt.plot(history.history['val_accuracy'], label='accuracy on validation data')
plt.legend()
plt.show()

#Loding the best saved model
model.load_weights(model_save_path)

#prepareing test date to evaluate the network
docs_test = nlp.pipe(reviews_test, batch_size=50, disable=["tagger", "parser", 'ner', 'textcat'])
int_sequence_test = [seq2int(doc, "token") for doc in docs_test]

#trimming reviews' lengh and filling short reviews with zeros
y_train = pad_sequences(int_sequence_test, maxlen=max_review_len)
print(len(int_sequence_test))

#testing model with new data
model.evaluate(y_train, y_test, verbose=True)

# random reviews copied from yelp.com
my_review_5_stars = """This place responds so well to patrons' requests. They are serving their takeout in the eco containers  - very important if you are accustomed to take out. The food is delicious. Fried oyster panini is my personal fave. Good job! Will recommend to anybody."""
my_review_3_stars = """Across from the Moxy hotel sits Da Claudio. I've eaten here 3 times & the wait staff is excellent. I came here today with a large party and overall the experience was good. I ordered the Eggplant Parmesan and it was ok & was $22.00 on the lunch menu. The lunch menu is not cheap but it is average for the neighborhood & the portions are a nice size considering the cost. I enjoy the staff more than the food here but I prefer more hearty heavy Italian food & this is not that kind of restaurant. I might go again & try a different meal."""
my_review_1_stars = """I came all the way from Vegas to get the door shut in my face, because I was told I needed reservation by a blond guy. He was extremely rude and left me talking to myself, when he shut the door in my face"""

# application of the network on randomly selected reviews
for my_review in [my_review_5_stars, my_review_3_stars, my_review_1_stars]:
    my_sequence = seq2int(my_review, "string")
    data = pad_sequences([my_sequence], maxlen=max_review_len)
    result = model.predict(data)
    stars = 1 + np.argmax(result)
    print(stars, 'stars to this review')