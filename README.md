# Classifier for Yelp reviews based on LSTM neurons #

This repository was created to display my solution for the task from the course Neural Network class (https://www.youtube.com/watch?v=NPJ1kF-63VI)

### Task ###

Yelp full review dataset presents couples review and number of stars (From 1 to 5) provided by Yelp's users. The task is to design and train the neuron network to classify the review to one of the five groups (stars).

### What is happening inside ###

Here is an example of a review: 

The big part of the task is to prepare data for the model. Here are steps I followed to prepare reviews for the network:

* At the beginning I am using spacy to split reviews to tokens (words, punctuation mark and so).
* After that  I create a dictionary with the most frequent tokens from all reviews.
* Then I map tokenized reviews to the numerical code of a token from the dictionary.
* At the end I trim all reviews or fill them with zeros so all of then have the same length

At this point data is ready to be processed by the model. Here is the architecture of the neural network:

Model: sequential_1

| Layer (type)       | Output Shape     | Param #       |
| :------------- | :----------: | -----------: |
|  embedding (Embedding)   | (None, 150, 100)     |  1300100    |
|  spatial_dropout1d (SpatialDr   | (None, 150, 100)     |  0    |
|  lstm (LSTM)   | (None, 150, 40)     |  22560    |
|  lstm_1 (LSTM)   | (None, 40)     |  12960    |
|  dense (Dense)   | (None, 5)     |  205    |

Total params: 1,335,825
Trainable params: 1,335,825
Non-trainable params: 0
	

### Sources and Inspirations ###

* The data set - https://course.fast.ai/datasets
* Architecture was inspired by this solution - https://www.kaggle.com/c/jigsaw-toxic-comment-classification-challenge/discussion/52644


### Who do I talk to? ###

The executable code is hosted in Google Colab - https://colab.research.google.com/drive/1LuqZL6vsc-QdAjxUxwKvJbsKD4dFrpvS?usp=sharing